from templates import templatebuild as tp
import asyncio

dbx = tp.create_db_connection("jdbc:sqlite:stcmdb.sqlite")
print(f"loading database at {dbx}")


def query_db(db, table):
    """
    query dbx database with the table you want (selects ALL)

    :param   db:      pass dbx (line 4)
    :param   table:   table in dbx
    :return: nothing
    """
    with db:
        tp.select_all_db(db, table)


class SpecialUserComponentTemplate(tp.SpecializedPartTemplate):
    def __init__(self, partname):
        super().__init__(n=partname)
        self.linkedComponents = []


query_db(dbx, "components")