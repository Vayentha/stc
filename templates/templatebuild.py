import os
import sys
import random
import sqlite3
from sqlite3 import Error

class GenericPartTemplate:
    def __init__(self, name):
        self.component_name = name
        self.component_number = int(random.random() * 10000)
        self.utility = []
        self.datasheets = []
        self.part_cost = ""
        self.source_sites = []
        self.cheapest_part = False

    def add_utility(self, util):
        self.utility.append(util)

    def add_datasheet(self, ds):
        self.datasheets.append(ds)

    def get_costs(self, costs):
        self.part_cost = costs

    def add_source(self, src):
        self.source_sites.append(src)

    def find_cheaper_equiv_parts(self):
        return


class SpecializedPartTemplate(GenericPartTemplate):
    def __init__(self, n):
        super().__init__(name=n)
        self.build_configs = []
        self.linked_project = []

    def print_all_component_info(self):
        print(f"component name   --> {self.component_name}")
        print(f"component number --> {self.component_number}")
        print(f"known utilities  --> {self.utility}")




def create_db_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")
    print(connection)
    return connection


def create_component_sql(conn, dbf):
    """
    add a component to the components database in sqlite


    :param dbf:
    :return: true if component works, false if component cannot be added
    """
    sql = f''' INSERT INTO components(name, part_number, jjidx)
    VALUES (?, {int(random.random() * 1000)}, ?) '''
    cur = conn.cursor()
    cur.execute(sql, dbf)
    return cur.lastrowid

def create_project_sql(conn, dbf):
    sql = ''' INSERT INTO projects(pname, pid, powner, completiontime) 
    VALUES (?, ?, ?, ?) '''
    cur = conn.cursor()
    cur.execute(sql, dbf)
    return cur.lastrowid

def create_product_sql(conn, plink):
    """

    create a product in the products table
    :param conn:  db connection
    :param plink: products
    :return: last row
    """
    sql = ''' INSERT INTO products(productName, revisionNum, dateMade, gitlabLink, prodNum, releaseDate)
     VALUES (?, ?, ?, ?, ?, ?)'''
    cur = conn.cursor()
    cur.execute(sql, plink)
    return cur.lastrowid

def select_all_db(conn, table):
    """

    :return:
    """
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM {table}")
    rows = cur.fetchall()
    for row in rows:
        print(row)
    return



#gb = SpecializedPartTemplate("garbo")
#gb.add_utility("nog")
#gb.print_all_component_info()
#dbx = create_db_connection("jdbc:sqlite:stcmdb.sqlite")
#with dbx:
    # trow = ("hohan2", 76)
    # pgid = create_component_sql(dbx, trow)
    # select_all_db(dbx, "components")
    # proj1 = ("astraswarm", "1.1.0", "2020-1-3", "gitlab.com/Vayentha/astraswarm", 1, "2030-1-1" )
    # prodd = create_product_sql(dbx, proj1)
    # select_all_db(dbx, "products")



